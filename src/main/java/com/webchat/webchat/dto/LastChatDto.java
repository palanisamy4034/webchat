package com.webchat.webchat.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LastChatDto {
    private int chatId;
    private int reciverId;
    private String EMail;
    private String dispName;
    private ChatDtl dtl;
}
