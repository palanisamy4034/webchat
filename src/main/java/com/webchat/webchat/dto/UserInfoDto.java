package com.webchat.webchat.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class UserInfoDto {
        private Integer id;
        private String userName;
        private String mobileNo;
        private String eMail;
        private String avatar;
}

