package com.webchat.webchat.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ChatResponseDto {
    private int chatId;
    @CreatedDate
    private Date senderTime;
    private List<ChatDtl> dtl;
}
