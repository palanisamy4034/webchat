package com.webchat.webchat.dto;

import com.webchat.webchat.model.ChatMessage;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
public class ChatMessageDto {
    private Integer ChatId;
    private ChatMessage.MessageType msgType;
    private String msgContent;
    private Date sendTime;
    private int senderId;

}
