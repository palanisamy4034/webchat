package com.webchat.webchat.dto;

import com.webchat.webchat.model.ChatMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChatDtl {
    private int id;
    private ChatMessage.MessageType msgType;
    private String msgContent;
    private Date sendTime;
    private Date reciverTime;
    private Date seenTime;
    private int senderId;
}
