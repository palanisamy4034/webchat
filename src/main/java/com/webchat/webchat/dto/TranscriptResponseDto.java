package com.webchat.webchat.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TranscriptResponseDto {
    private List<TranscriptDto> transcriptDto;
}
