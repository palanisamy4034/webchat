package com.webchat.webchat.dto;

import com.webchat.webchat.model.ChatMessage;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TranscriptDto {
    private int chatId;
    private String msg;
    private ChatMessage.MessageType msgType;
    private String lastSeen;
    private int unreadMessages;
    private List<ChatDtl> chatDtlList;

}
