package com.webchat.webchat.service;

import com.webchat.webchat.model.ConvModel;
import com.webchat.webchat.repository.ConvRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConvService {

    @Autowired
    ConvRepository convRepository;

    public ConvModel getConvId(ConvModel convModel) {
        if ((convModel.getReciverId() != null && convModel.getSenderId() != null)
                && (convModel.getReciverId() != convModel.getSenderId())
                && (convModel.getReciverId() != 0 && convModel.getSenderId() != 0)) {
            ConvModel convModel1 = convRepository.findBySenderIdAndReciverId(convModel.getSenderId(), convModel.getReciverId());
            if (convModel1 == null) {
                ConvModel convModel2 = convRepository.save(convModel);
                return convModel2;
            }
            return convModel1;
        }
        return convModel;
    }
}