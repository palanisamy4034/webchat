package com.webchat.webchat.service;

import com.webchat.webchat.dto.*;
import com.webchat.webchat.model.ChatMessage;
import com.webchat.webchat.model.ConvModel;
import com.webchat.webchat.model.UserInfo;
import com.webchat.webchat.repository.ChatRepository;
import com.webchat.webchat.repository.ConvRepository;
import com.webchat.webchat.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Component
public class ChatService {
    private static final Logger logger = Logger.getLogger("ChatService.class");
    @Autowired
    ChatRepository chatRepository;

    @Autowired
    ConvRepository convRepository;

    @Autowired
    UserRepository userRepo;

    public ChatResponseDto sendMessage(ChatMessageDto chatResponseDto) {

        ChatMessage chatMessage = new ChatMessage(chatResponseDto.getChatId(), chatResponseDto.getMsgType(), chatResponseDto.getMsgContent(), chatResponseDto.getSenderId());
        chatRepository.save(chatMessage);

        ChatResponseDto response = new ChatResponseDto();
        response.setChatId(chatMessage.getChatId());

        ChatDtl chatDtl = new ChatDtl(chatMessage.getId(), chatMessage.getMsgType(),
                chatMessage.getMsgContent(), chatMessage.getSendTime(),
                chatMessage.getReciverTime(), chatMessage.getSeenTime(), chatMessage.getSenderId());
        ArrayList<ChatDtl> chatDtlList = new ArrayList<>();
        chatDtlList.add(chatDtl);
        response.setDtl(chatDtlList);
        return response;
    }

    public TranscriptResponseDto getMessages(int senderId) {
        List<ConvModel> convModel = convRepository.findAllConversationIdBySenderId(senderId);


        if (convModel == null) {
            return null;
        }

        List<TranscriptDto> transcriptDtos = new ArrayList<>();
        for (ConvModel cm : convModel) {

            TranscriptDto transcriptDto = new TranscriptDto();
            transcriptDto.setChatId(cm.getId());

            List<ChatMessage> chatMessages = chatRepository.findByChatId(cm.getId());
            List<ChatDtl> chatDtlList = new ArrayList<>();
            for (ChatMessage chatMessage : chatMessages) {
                ChatDtl chatDtl = new ChatDtl();
                chatDtl.setId(chatMessage.getId());
                chatDtl.setSenderId(chatMessage.getSenderId());
                chatDtl.setMsgContent(chatMessage.getMsgContent());
                chatDtl.setMsgType(chatMessage.getMsgType());
                chatDtl.setSeenTime(chatMessage.getSeenTime());
                chatDtl.setSendTime(chatMessage.getSendTime());
                chatDtl.setReciverTime(chatMessage.getReciverTime());
                chatDtlList.add(chatDtl);
            }
            transcriptDto.setChatDtlList(chatDtlList);
            transcriptDtos.add(transcriptDto);
        }

        TranscriptResponseDto response = new TranscriptResponseDto();
        response.setTranscriptDto(transcriptDtos);

        return response;
    }

    public TranscriptDto getConversationByChatId(int chatId) {
        List<ChatMessage> chatMessages = chatRepository.findByChatId(chatId);
        List<ChatDtl> chatDtlList = new ArrayList<>();
        TranscriptDto transcriptDto = new TranscriptDto();
        transcriptDto.setChatId(chatId);

        for (ChatMessage chatMessage : chatMessages) {
            ChatDtl chatDtl = new ChatDtl();
            chatDtl.setSenderId(chatMessage.getSenderId());
            chatDtl.setId(chatMessage.getId());
            chatDtl.setMsgContent(chatMessage.getMsgContent());
            chatDtl.setMsgType(chatMessage.getMsgType());
            chatDtl.setSeenTime(chatMessage.getSeenTime());
            chatDtl.setSendTime(chatMessage.getSendTime());
            chatDtl.setReciverTime(chatMessage.getReciverTime());
            chatDtlList.add(chatDtl);
        }
        transcriptDto.setChatDtlList(chatDtlList);

        return transcriptDto;
    }

    public List<LastChatDto> getLastMsgByChatId(int senderId) {

        List<ConvModel> convModel = convRepository.findAllConversationIdBySenderId(senderId);
        ChatResponseDto response = new ChatResponseDto();

        List<LastChatDto> lastChatDto = new ArrayList<>();
        for (ConvModel cm : convModel) {
            ChatMessage chatMessage = chatRepository.findLastMsgByChatId(cm.getId());
            if(chatMessage==null){
                continue;
            }
            int reciverId=cm.getSenderId() == senderId ? cm.getReciverId():cm.getSenderId();
            UserInfo userInfo = userRepo.findByid(reciverId);
            LastChatDto lastChatDto1 = new LastChatDto();
            ChatDtl dtl = new ChatDtl();
            dtl.setMsgContent(chatMessage.getMsgContent());
            dtl.setMsgType(chatMessage.getMsgType());
            dtl.setId(chatMessage.getId());
            dtl.setSendTime(chatMessage.getSendTime());
            dtl.setSenderId(chatMessage.getSenderId());
            lastChatDto1.setChatId(cm.getId());
            lastChatDto1.setDtl(dtl);
            lastChatDto1.setReciverId(reciverId);
            lastChatDto1.setEMail(userInfo.getEMail());
            lastChatDto1.setDispName(userInfo.getUserName());
            lastChatDto.add(lastChatDto1);
        }
        return lastChatDto;
    }


}

