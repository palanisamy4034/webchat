package com.webchat.webchat.service;

import com.webchat.webchat.dto.UserInfoDto;
import com.webchat.webchat.model.ResponseClass;
import com.webchat.webchat.model.UserInfo;
import com.webchat.webchat.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserInfoService {
    @Autowired
    UserRepository userRepo;

    public ResponseClass addUser(UserInfo userInfo) {

        UserInfo userInfo1 = userRepo.findByEMail(userInfo.getEMail());
        ResponseClass responseClass = new ResponseClass();
        if (userInfo1 == null) {
            userInfo1 = userRepo.save(userInfo);
            responseClass.setStatus(true);
            responseClass.setMsg("New customer");
            responseClass.setUserName(userInfo1.getUserName());
            responseClass.setId(userInfo1.getId());
        } else {
            responseClass.setStatus(true);
            responseClass.setMsg("Existing customer");
            responseClass.setUserName(userInfo1.getUserName());
            responseClass.setId(userInfo1.getId());
        }
        return responseClass;
    }

    public ResponseClass getUser(UserInfo userInfo) {

        UserInfo userInfo1 = userRepo.findByEMail(userInfo.getEMail());
        ResponseClass responseClass = new ResponseClass();
        if (userInfo1 == null) {
            responseClass.setStatus(false);
            responseClass.setMsg("failure");
        } else {
            responseClass.setStatus(true);
            responseClass.setMsg("Sucess");
            responseClass.setUserName(userInfo1.getUserName());
            responseClass.setId(userInfo1.getId());
        }
        return responseClass;

    }

    public ResponseClass userLogin(UserInfo userInfo) {

        UserInfo userInfo1 = userRepo.findByEMail(userInfo.getEMail());
        ResponseClass responseClass = new ResponseClass();
        if (userInfo1 == null) {
            responseClass.setStatus(false);
            responseClass.setMsg("Invalid Email-Id");
            responseClass.setEMail(userInfo.getEMail());
        } else if (userInfo1.getPassword().equalsIgnoreCase(userInfo.getPassword())) {
            responseClass.setStatus(true);
            responseClass.setMsg("Existing customer");
            responseClass.setUserName(userInfo1.getUserName());
            responseClass.setId(userInfo1.getId());
        } else {
            responseClass.setStatus(true);
            responseClass.setMsg("Password Mismatch");
            responseClass.setEMail(userInfo.getEMail());

        }
        return responseClass;

    }

    public Object getAllUser() {

        List<UserInfo> userInfos = userRepo.findAll();
        List<UserInfoDto> userInfoDtoList = new ArrayList<>();
        for (UserInfo userInfo : userInfos) {
            UserInfoDto userInfoDto = new UserInfoDto();
            userInfoDto.setEMail(userInfo.getEMail());
            userInfoDto.setUserName(userInfo.getUserName());
            userInfoDto.setAvatar(userInfo.getAvatar());
            userInfoDto.setId(userInfo.getId());
            userInfoDtoList.add(userInfoDto);
        }
        return userInfoDtoList;
    }
}
