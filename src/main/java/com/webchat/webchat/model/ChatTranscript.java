package com.webchat.webchat.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChatTranscript extends UserInfo {
    private ChatMessage chatMessage;
}
