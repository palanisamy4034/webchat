package com.webchat.webchat.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="chat_user_trans")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class ChatMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "id")
    private Integer id;

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "chat_id")
    private Integer chatId;

    private MessageType msgType;
    private String msgContent;
    private int senderId;

    @CreatedDate
    private Date sendTime;

    private Date reciverTime;
    private Date seenTime;

    public enum MessageType {
        CHAT,
        JOIN,
        LEAVE
    }

    public ChatMessage(Integer chatId, MessageType msgType, String msgContent, int senderId) {
        this.chatId = chatId;
        this.msgContent = msgContent;
        this.msgType = msgType;
        this.senderId = senderId;
    }

}
