package com.webchat.webchat.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name="chat_user_dtl")
@Getter
@Setter
@ToString

public class UserInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "id")
    private Integer id;
    private String userName;
    @JsonIgnore
    private String mobileNo;
    private String eMail;
    private String avatar;
    private String password;
}