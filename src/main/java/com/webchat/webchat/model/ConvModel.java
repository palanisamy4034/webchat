package com.webchat.webchat.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name="chat_conv_dtl")
@Getter
@Setter
@ToString

public class ConvModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "chatId")
    private Integer id;

    private Integer senderId;
    private  Integer reciverId;

}
