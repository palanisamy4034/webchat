package com.webchat.webchat.controller;

import com.webchat.webchat.model.ResponseClass;
import com.webchat.webchat.model.UserInfo;
import com.webchat.webchat.repository.UserRepository;
import com.webchat.webchat.service.UserInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/chat")
public class UserInfoController {
    private static final Logger logger = LoggerFactory.getLogger(UserInfoController.class);
    @Autowired
    UserRepository userRepo;

    @Autowired
    UserInfoService userInfoService;

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public ResponseClass addUser(@RequestBody UserInfo userInfo) {

        return userInfoService.addUser(userInfo);
    }

    @RequestMapping(value = "/getUser", method = RequestMethod.POST)
    public ResponseClass getUser(@RequestBody UserInfo userInfo) {

        return userInfoService.getUser(userInfo);
    }


    @RequestMapping(value = "/getAllUser", method = RequestMethod.GET)
    public Object getAllUser() {

        return userInfoService.getAllUser();

    }

    @RequestMapping(value = "/userLogin", method = RequestMethod.POST)
    public ResponseClass userLogin(@RequestBody UserInfo userInfo) {

        return userInfoService.userLogin(userInfo);

    }


}