package com.webchat.webchat.controller;

import com.webchat.webchat.dto.*;
import com.webchat.webchat.model.ConvModel;
import com.webchat.webchat.repository.ChatRepository;
import com.webchat.webchat.repository.ConvRepository;
import com.webchat.webchat.repository.UserRepository;
import com.webchat.webchat.service.ChatService;
import com.webchat.webchat.service.ConvService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/chat")
public class ChatController {
    private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

    @Autowired
    ChatRepository chatRepository;
    @Autowired
    ConvRepository convRepository;
    @Autowired
    UserRepository userRepository;

    @Autowired
    ChatService chatService;

    @Autowired
    ConvService convService;

    @MessageMapping("/chat.sendMessage.{channelId}")
    @SendTo("/channel/public.{channelId}")
    public ChatResponseDto sendMessage(@Payload ChatMessageDto chatMessageDto) {
        return chatService.sendMessage(chatMessageDto);
    }

    @RequestMapping(value = "/convId", method = RequestMethod.POST)
    public Object convId(@RequestBody ConvModel convModel) {
        return convService.getConvId(convModel);
    }

    @RequestMapping(value = "/getMessages/{senderId}", method = RequestMethod.GET)
    public TranscriptResponseDto getMessages(@PathVariable("senderId") int senderId) {
        return chatService.getMessages(senderId);
    }

    @RequestMapping(value = "/getConv/{chatId}", method = RequestMethod.GET)
    public TranscriptDto getConversationByChatId(@PathVariable("chatId") int chatId) {
        return chatService.getConversationByChatId(chatId);
    }
    @RequestMapping(value = "/lastMsg/{senderId}", method = RequestMethod.GET)
    public List<LastChatDto> getLastMsgByChatId(@PathVariable("senderId") int senderId) {
        return chatService.getLastMsgByChatId(senderId);
    }
}
