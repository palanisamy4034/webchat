package com.webchat.webchat.repository;

import com.webchat.webchat.model.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatRepository extends JpaRepository<ChatMessage, Integer> {
    List<ChatMessage> findByChatId(Integer chatId);


    @Query("select cm from ChatMessage cm where"+
            " cm.id=(select max(cti.id) from ChatMessage cti where cti.chatId=:chatId)")
    ChatMessage findLastMsgByChatId(@Param("chatId") Integer chatId);


}
