package com.webchat.webchat.repository;

import com.webchat.webchat.model.ConvModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConvRepository extends JpaRepository<ConvModel, Integer> {
    @Query("select cm from ConvModel cm " +
            "where ((cm.senderId=:senderId and cm.reciverId=:reciverId) " +
            "or (cm.senderId=:reciverId and cm.reciverId=:senderId))")
    ConvModel findBySenderIdAndReciverId(@Param("senderId") Integer senderId, @Param("reciverId") Integer reciverId);

    @Query("select cm from ConvModel cm " +
            "where cm.senderId=:senderId or cm.reciverId=:senderId")
    List<ConvModel> findAllConversationIdBySenderId(@Param("senderId") Integer senderId);
}
