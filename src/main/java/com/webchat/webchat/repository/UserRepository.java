package com.webchat.webchat.repository;

import com.webchat.webchat.model.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserInfo, Integer>{
    UserInfo findByEMail(String eMail);

    UserInfo findByid(Integer Id);
}