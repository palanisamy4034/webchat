#common build

./gradlew build

#Fast Build

./gradlew build --dry-run --daemon

#Port Configured

8080

#Run the jar

java -jar build/libs/webchat(*).jar

#Import lombok on intellij

File > Settings > Plugins > Browse repositories... > Search for "lombok" > Install Plugin